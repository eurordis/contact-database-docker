# Contact Database Docker

## install

Create a folder to clone the projects:

```
mkdir contact-database && cd contact-database
git clone https://gitlab.com/eurordis/contact-database-docker docker
git clone https://gitlab.com/eurordis/contact-database api
git clone https://gitlab.com/eurordis/contact-database-client client
```

Run the instances
```
cd docker
docker compose up -d
```

> Note that the docker compose is configured by default to match the folders
> structure as mentioned above.

Open your browser at http://localhost:8080
